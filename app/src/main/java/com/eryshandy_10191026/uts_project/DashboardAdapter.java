package com.eryshandy_10191026.uts_project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardHolder> {

    private ArrayList<SetterGetter> listdata;

    public DashboardAdapter(ArrayList<SetterGetter> listdata) {
        this.listdata = listdata;
    }

    @NonNull
    @Override
    public DashboardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent,false);
        DashboardHolder holder = new DashboardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardHolder holder, int position) {

        final SetterGetter getData = listdata.get(position);
        String judulMenu = getData.getJudul();
        String gambarMenu = getData.getGambar();

        holder.judulMenu.setText(judulMenu);
        if(gambarMenu.equals("logo1")) {
            holder.gambarMenu.setImageResource(R.drawable.search);
        } else if(gambarMenu.equals("logo2")) {
            holder.gambarMenu.setImageResource(R.drawable.gmail);
        } else if(gambarMenu.equals("logo3")) {
            holder.gambarMenu.setImageResource(R.drawable.maps);
        } else if(gambarMenu.equals("logo4")) {
            holder.gambarMenu.setImageResource(R.drawable.business);
        } else if(gambarMenu.equals("logo5")) {
            holder.gambarMenu.setImageResource(R.drawable.docs);
        } else if(gambarMenu.equals("logo6")) {
            holder.gambarMenu.setImageResource(R.drawable.drive);
        } else if(gambarMenu.equals("logo7")) {
            holder.gambarMenu.setImageResource(R.drawable.adwords);
        } else if(gambarMenu.equals("logo8")) {
            holder.gambarMenu.setImageResource(R.drawable.sheets);
        } else if(gambarMenu.equals("logo9")) {
            holder.gambarMenu.setImageResource(R.drawable.translate);
        } else if(gambarMenu.equals("logo10")) {
            holder.gambarMenu.setImageResource(R.drawable.news);
        } else if(gambarMenu.equals("logo11")) {
            holder.gambarMenu.setImageResource(R.drawable.slides);
        } else if(gambarMenu.equals("logo12")) {
            holder.gambarMenu.setImageResource(R.drawable.youtube);
        } else if(gambarMenu.equals("logo13")) {
            holder.gambarMenu.setImageResource(R.drawable.calendar);
        } else if(gambarMenu.equals("logo14")) {
            holder.gambarMenu.setImageResource(R.drawable.photos);
        } else if(gambarMenu.equals("logo15")) {
            holder.gambarMenu.setImageResource(R.drawable.gplus);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashboardHolder extends RecyclerView.ViewHolder {

        TextView judulMenu;
        ImageView gambarMenu;

        public DashboardHolder(@NonNull View itemView) {
            super(itemView);

            judulMenu = itemView.findViewById(R.id.title_menu);
            gambarMenu = itemView.findViewById(R.id.logo_menu);
        }
    }
}
