package com.eryshandy_10191026.uts_project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> dataapp;
    GridLayoutManager gridLayoutManager;
    DashboardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.Dashboard);

        addData();
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashboardAdapter(dataapp);
        recyclerView.setAdapter(adapter);
    }

    public void addData() {

        dataapp = new ArrayList<>();
        dataapp.add(new SetterGetter("Search", "logo1"));
        dataapp.add(new SetterGetter("Gmail", "logo2"));
        dataapp.add(new SetterGetter("Maps", "logo3"));
        dataapp.add(new SetterGetter("Business", "logo4"));
        dataapp.add(new SetterGetter("Docs", "logo5"));
        dataapp.add(new SetterGetter("Drive", "logo6"));
        dataapp.add(new SetterGetter("Adwords", "logo7"));
        dataapp.add(new SetterGetter("Sheets", "logo8"));
        dataapp.add(new SetterGetter("Translate", "logo9"));
        dataapp.add(new SetterGetter("News", "logo10"));
        dataapp.add(new SetterGetter("Slide", "logo11"));
        dataapp.add(new SetterGetter("Youtube", "logo12"));
        dataapp.add(new SetterGetter("Calendar", "logo13"));
        dataapp.add(new SetterGetter("Photos", "logo14"));
        dataapp.add(new SetterGetter("G-plus", "logo15"));
    }
}