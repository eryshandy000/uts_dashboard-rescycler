package com.eryshandy_10191026.uts_project;

public class SetterGetter {
    String judul;
    String gambar;

    public SetterGetter(String judul, String gambar) {
        this.judul = judul;
        this.gambar = gambar;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
